import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BandListComponent } from './components/bandlist/bandlist.component';
import { EditFormComponent } from './components/edit-form/edit-form.component';
import { NewFormComponent } from './components/new-form/new-form.component';

const routes: Routes = [
  { path: '', component: BandListComponent },
  { path: 'new', component: NewFormComponent },
  { path: 'edit/:id', component: EditFormComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
