import { IBand } from './../../interfaces/newinterface';
import { BandServiceService } from './../../services/band-service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './bandlist.component.html',
  styleUrls: ['./bandlist.component.scss']
})
export class BandListComponent implements OnInit {
  bands: IBand[];

  constructor(private bandServiceService: BandServiceService) {
    this.bands = this.bandServiceService.getBands();
  }

  ngOnInit() {}

  onClickDeleteBand(id: number) {
    this.bandServiceService.setDeleteBand(id);
  }
}