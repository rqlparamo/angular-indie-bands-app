import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { IBand } from './../../interfaces/newinterface';
import { BandServiceService } from './../../services/band-service.service';

@Component({
  selector: 'app-new-form',
  templateUrl: './new-form.component.html',
  styleUrls: ['./new-form.component.scss']
})
export class NewFormComponent implements OnInit {
  constructor(private bandServiceService: BandServiceService, private router: Router) {}

  ngOnInit() {}

  // Create the band and navigate home after it resolves
  onFormSubmit(band: IBand) {
    this.bandServiceService.setCreateBand(band)
      .then(() => {
        this.router.navigate(['/']);
      })
  }
}