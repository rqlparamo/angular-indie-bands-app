import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup} from '@angular/forms';
import { IBandForm } from 'src/app/interfaces/FormInterface';
import { IBand } from 'src/app/interfaces/newinterface';
import uuid from 'uuid';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  @Input() id: number | string = null;

  @Input() image: string = '';
  @Input() name: string = '';
  @Input() bio: string = '';
  @Input() members: string = '';
  @Input() source: string = '';

  @Output() formHandler = new EventEmitter<IBand>();

  bandForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    // Build the form using the input values as the initial values
    this.bandForm = this.formBuilder.group({
      image: [this.image],
      name: [this.name],
      bio: [this.bio],
      members: [this.members],
      source: [this.source]
    });
  }

  formatFormValues(values: IBandForm): IBand {
    return {
      id: this.id !== null ? this.id : uuid(), // If no id, generate a random id
      image: this.image = undefined
      ? this.image : 'https://st-listas.20minutos.es/images/2012-08/340605/list_640px.jpg?1519227306', // if no image apply default one
      info: {
        name: values.name,
        bio: values.bio,
        members: values.members,
        source: values.source,
      },

    };
  }

  onFormSubmit(values: IBandForm) {
    const bandValue: IBand = this.formatFormValues(values);
    this.formHandler.emit(bandValue);
  }
}
