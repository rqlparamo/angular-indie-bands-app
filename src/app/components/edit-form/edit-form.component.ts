import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { IBand } from './../../interfaces/newinterface';
import { BandServiceService } from './../../services/band-service.service';

@Component({
  selector: 'app-edit-form',
  templateUrl: './edit-form.component.html',
  styleUrls: ['./edit-form.component.scss']
})
export class EditFormComponent implements OnInit {
  band: IBand;

  constructor(
    private bandServiceService: BandServiceService,
    private activeRouter: ActivatedRoute,
    private router: Router
  ) {
    this.band = null;
  }

  ngOnInit() {

    this.activeRouter.paramMap.subscribe(params => {
      const id = params.get('id');
      this.band = this.bandServiceService.getbandById(id);
    });
  }

  onFormSubmit(band: IBand) {
    this.bandServiceService.setEditBand(band).then(() => {
      this.router.navigate(['/']);
    });
  }
}
