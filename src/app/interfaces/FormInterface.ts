export interface IBandForm {
    id: number;
    image: string;
    name: string;
    bio: string;
    members: string;
    source: string;
  }