interface IInfos {
name: string;
bio: string;
members: string;
source: string;
}

export interface IBand {
  id: number;
  image: string;
  info: IInfos;
}
