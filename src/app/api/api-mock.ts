import {IBand} from './../interfaces/newinterface';


const response: IBand [] = [
  {
    id: 0,
    image: 'https://static1.lavozdigital.es/media/cultura/2018/08/30/muse-k3oD--620x349@abc.jpg',
    info: {
      name: 'Muse',
      bio: 'Muse are an English rock band from Teignmouth, Devon, formed in 1994.',
      members: 'Matt Bellamy, Chris Wolstenholme and Dominic Howard (drums).',
      source: 'https://en.wikipedia.org/wiki/Muse_(band)'
    }
  },
  {
    id: 1,
    image: 'https://i0.wp.com/revistasoyrock.com.ar/wp/wp-content/uploads/2018/08/franz.jpg?fit=1200%2C799&ssl=1',
    info: {
      name: 'Franz Ferdinand',
      bio: 'Franz Ferdinand are a Scottish rock band formed in Glasgow in 2002.',
      members: 'Alex Kapranos, Bob Hardy, Nick McCarthy and Paul Thomson .',
      source: 'https://en.wikipedia.org/wiki/Franz_Ferdinand_(band)'
    }
  },
  {
    id: 2,
    image: 'https://media.pitchfork.com/photos/592c6d92d7a71d1ea569d580/2:1/w_648/c1481165.jpg',
    info: {
      name: 'Interpol',
      bio: 'nterpol is an American rock band from Manhattan, New York, formed in 1997. ',
      members: 'Paul Banks, Carlos Dengler, Daniel Kessler, Sam Fogarino, Greg Drudy and Pajó.',
      source: 'https://en.wikipedia.org/wiki/Interpol_(band)'
    }
  },

  {
    id: 3,
    image: 'https://www.nacionrock.com/wp-content/uploads/qotsaf.png',
    info: {
      name: 'Queens of the Stone Age',
      bio:
        'Queens of the Stone Age (commonly abbreviated QOTSA) is an American rock band formed in 1996 in Palm Desert, California.',
      members:
        'Josh Homme, Troy Van Leeuwen, Dean Fertita, Michael Shuman, Jon Theodore and Dave Grohl.',
      source: 'https://en.wikipedia.org/wiki/Queens_of_the_Stone_Age'
    }
  },
  {
    id: 4,
    image: 'https://culto.latercera.com/wp-content/uploads/2019/10/Kaiser-Chiefs-web.jpg',
    info: {
      name: 'Kaiser Chiefs',
      bio:
        'Kaiser Chiefs are an English indie rock[1][2] band from Leeds who formed in 2000 as Parva.',
      members: 'Ricky Wilson, Andrew White, Simon Rix, Nick Baines and Vijay Mistry.',
      source: 'https://en.wikipedia.org/wiki/Kaiser_Chiefs'
    }
  },
  {
    id: 5,
    image: 'https://culto.latercera.com/wp-content/uploads/2017/05/phoenix-900x600.jpg',
    info: {
      name: 'Phoenix',
      bio: 'Phoenix is an indie pop band from Versailles, France',
      members: 'Thomas Mars, Deck dArcy, Laurent Brancowitz and Christian Mazzalai.',
      source: 'https://en.wikipedia.org/wiki/Phoenix_(band)'
    }
  }
];

export const getApiResponse = async (): Promise<IBand[]> => {
  return response;
};


