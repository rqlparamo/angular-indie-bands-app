import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BandListComponent } from './components/bandlist/bandlist.component';
import { FormComponent } from './components/form/form.component';
import { EditFormComponent } from './components/edit-form/edit-form.component';
import { NewFormComponent } from './components/new-form/new-form.component';

@NgModule({
  declarations: [AppComponent, BandListComponent, FormComponent, EditFormComponent, NewFormComponent],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
