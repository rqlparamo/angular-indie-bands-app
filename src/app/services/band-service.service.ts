import { Injectable } from '@angular/core';
import { IBand } from '../interfaces/newinterface';

import { getApiResponse } from './../api/api-mock';

@Injectable({
  providedIn: 'root'
})
export class BandServiceService {
  public bands: IBand[];

  constructor() {
    this.bands = []; // build service only
    this.getInitialResponse();
  }
  getInitialResponse(): void {
    getApiResponse().then((response: IBand[]) => {
      response.forEach((band: IBand) => {
        this.bands.push(band);
      });
    });
  }

  public getBands(): IBand[] {
    return this.bands;
  }

  private checkIds(firstId: string | number, secondId: string | number): boolean {
    return firstId.toString() === secondId.toString();
  }

  // Find a band by id in the array and return it
  public getbandById(id: number | string): IBand {
    return this.bands.find((band: IBand) => this.checkIds(band.id, id));
  }

  // Push a new band to the bands array
  public setCreateBand(newBand: IBand): Promise<boolean> {
    this.bands.push(newBand);

    // Return a promise so we can control when the band is deleted
    return Promise.resolve(true);
  }

  // Find a band with the same id of the edited one change it's whole value
  public setEditBand(editedBand: IBand): Promise<boolean> {
    this.bands.forEach((band: IBand, index: number) => {
      if (this.checkIds(band.id, editedBand.id)) {
        this.bands[index] = editedBand;
      }
    });

    // Return a promise so we can control when the band is deleted
    return Promise.resolve(true);
  }

  // Find the index of a band by its id and remove it from the array with splice
  public setDeleteBand(id: number | string): void {
    const indexOfBand = this.bands.findIndex(band => this.checkIds(band.id, id));
    this.bands.splice(indexOfBand, 1);
  }
}
